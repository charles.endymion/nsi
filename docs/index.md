#  Espace NSI

* [Projet semi-guidé : le jeu du démineur](https://trieles.gitlab.io/nsi/10-demineur/){target='__blank' rel='noopener'}.

* [Introduction à Tkinter](https://trieles.gitlab.io/nsi/20-tkinter/){target='__blank' rel='noopener'}.  
Ce document (hors-programme de la spécialité NSI) pourra néanmoins être
utile (par exemple dans le cadre d'un projet).

* [Comment réaliser des petits jeux d'arcade avec Tkinter ?](https://trieles.gitlab.io/nsi/30-tkinter-pour-jouer/){target='__blank' rel='noopener'}  
Ce document (également hors-programme de la spécialité NSI) présente l'ensemble
des difficultés auxquelles un néophyte en programmation avec Tkinter devra
faire face s'il veut réaliser un petit jeu d'arcade (Pong, Casse-Briques,
Tetris, etc.). Les solutions adaptées ainsi que quelques exemples sont également
de mise, bien sûr :wink: !


![Démineur en mode console](images/rendu-console.png){width=600}

# Le puzzle de l'escalier qui monte

Imaginez un escalier à n marches. 
Lorsque vous montez cet escalier, vous montez une ou deux marches à la fois. 
Le but de ce casse-tête informatique est de découvrir, à l'aide d'un algorithme, 
de combien de manières distinctes pouvez-vous grimper au sommet ?

Prenons un exemple où vous montez un petit escalier de seulement 4 marches. 
N'oubliez pas que pour chaque "pas" que vous faites, vous pouvez monter 1 ou 2 marches à la fois. 

Ce qui signifie qu'il y a 5 façons différentes de monter cet escalier comme indiqué sur les images ci-dessous :

![escalier1](images/escalier1.png)
![escalier2](images/escalier2.png)
![escalier3](images/escalier3.png)
![escalier4](images/escalier4.png)
![escalier5](images/escalier5.png)


# Une approche récursive ? 

Étudions ce problème, "étape par étape" !!
On peut supposer que n, le nombre total de marches de l'escalier est un entier positif.
La sortie de notre algorithme est le nombre total de façons distinctes de monter cet escalier.

On peut donc facilement en déduire que :

- Si n = 0, la sortie doit également être nulle.

- Si n = 1, la sortie sera 1 (il n'y a qu'un moyen de gravir cette étape).

![escalier6](images/rec1.png)

- Si n = 2, la sortie sera 2 (il n'y a que deux façons de gravir cette étape).

![escalier7](images/rec2.png)
![escalier8](images/rec3.png)

Pour tout nombre n d'étapes supérieur à 2, on remarque que pour atteindre la n ième , 
il faut d'abord atteindre soit l'étape (n-1) soit l'étape (n-2).

![escalier9](images/rec4.png)

Donc, pour compter le nombre de façons dont nous pouvons atteindre l'étape n, 
nous pouvons utiliser la formule récursive suivante :

compte_chemin(1) = 1

compte_chemin(2) = 2

si n>2 alors compte_chemin(n) = compte_chemin(n-1) + compte_chemin(n-2)

où compte_chemin(n) est une fonction qui renvoie le nombre de façons dont vous peut atteindre la n ième marche.

# Un algorithme récursif

``` Python
def compte_chemin(n):
  if n==1:
    return 1
  elif n==2:
    return 2
  else:
    return compte_chemin(n-1) + compte_chemin(n-2)
    
marches = 10    
chemins = compte_chemin(marches)    
print("On dénombre " + str(chemins) + " distincts pour monter un escalier de  " + str(marches) + " marches.")
```

# Visualisation 

![visualisation](visualisation.png)

https://www.recursionvisualizer.com/?function_definition=def%20compter_chemins(n)%3A%0A%20%20if%20n%3D%3D1%3A%0A%20%20%20%20return%201%0A%20%20elif%20n%3D%3D2%3A%0A%20%20%20%20return%202%0A%20%20else%3A%0A%20%20%20%20return%20compter_chemins(n-1)%20%2B%20compter_chemins(n-2)&function_call=compter_chemins(5)
