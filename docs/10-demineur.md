# le jeu du démineur

Pour découvrir le jeu et les principes qui le régissent, je vous demande
de consulter **à l'avance** [la page de Wikipedia consacrée au jeu][demineur]{target=__blank rel='noopener'}.
Vous devez être familiarisé avec le jeu (je vous recommande même de faire
quelques parties ! si si !) : c'est **essentiel** pour bien comprendre la
logique sous-jacente, les mécanismes du jeu, ainsi que les possibilités
d'action du joueur.

L'objectif de ce document est de vous guider vers la réalisation d'un jeu
de démineur fonctionnel, d'abord en ligne de commandes (c'est l'objectif
principal), puis avec une véritable interface graphique (pour les volontaires
les plus rapides ou enthousiastes).


## Avant-propos

Ce projet est prévu pour être géré par deux personnes de niveaux différents,
qui devront donc collaborer pour le mener à terme. 2 séances de 2 heures
en classe, avec du travail personnel en sus, devraient suffire à venir à
bout de la partie 1. La partie 2 est optionnelle, à traiter par qui le souhaitera.

Ce projet est assez guidé : **dans un premier temps, vous devrez suivre le
canevas proposé.** Libre à vous, une fois l'objectif atteint, de l'aborder
à votre manière.

Bien sûr, rien n'empêche qui que ce soit de faire plus que ce qui est attendu.
Mais il importe que le résulat soit atteint (le jeu doit être fonctionnel
dans les temps). Le reste attendra.

 
## Partie 1 : réaliser un jeu qui fonctionne dans un terminal (en mode « ligne de commandes »)

### A. L'organisation

Vous trouverez ici un [code de base][base-code-demineur]{target=__blank rel='noopener'}, qu'il
vous faudra compléter. Le souci, c'est que travailler à deux impose à chacun
de coder dans un fichier qui lui est propre.

On suppose que les élèves portent les doux (pré)noms de A et B, et que B
se sent plus à l'aise en programmation Python que A.

* L'élève A recopiera depuis le [fichier de base][base-code-demineur]{target=__blank rel='noopener'}
le contenu qui le concerne dans le fichier [`A.py`][base-code-A]{target=__blank rel='noopener'}.

* L'élève B fera de même dans le fichier [`B.py`][base-code-B]{target=__blank rel='noopener'}. Il créera également un
fichier `demineur.py` qui importera les fichiers précédents :

    ``` python
    from A import *
    from B import *
    ...
    
    ```
    
    Ce fichier `demineur.py` contiendra la mécanique principale du jeu (initialisation
    du plateau de jeu, boucle infinie qui réalise jusqu'à l'issue du jeu — heureuse ou
    malheureuse — l'affichage, la demande de saisie, l'analyse de la saisie
    et les vérifications qui en déccoulent).

* Chacun des fichiers [`A.py`][base-code-A]{target=__blank rel='noopener'}. et [`B.py`][base-code-B]{target=__blank rel='noopener'}. comprendra,
après les imports, les définitions des constantes et des fonctions qui
conviennent, une section qui ne s'exécutera que si le script est *exécuté*,
et **non *importé***. Cela permettra à chacun de conduire des tests afin
de valider la pertinence de son travail.

    En Python, une telle section débute systématiquement par :
    
    ``` python
    if __name__ == "__main__":
        ...     # Ici le code pour mener des tests
    ```

* La personne A (celle qui se sent le moins à l'aise en programmation Python) devra
compléter le code des fonctions `lire`, `modifier`, `voisinage`, `analyse_voisinage`,
`place_mine_au_hasard`, `initialise_jeu`, `affiche_console` et `solution`.
Cela peut paraître beaucoup, mais la plupart de ces fonctions sont soit
courtes soit simples à coder.

* La personne B (celle qui se sent le plus à l'aise en programmation Python) devra
compléter le code des fonctions qui restent (`affiche_console` et `jeu_fini`)
ainsi que le code principal du jeu. Afin de l'aider à tester son code, elle
pourra utiliser [ce fichier « compilé » (pour Python 3.4.2)][A342.pyc]{target=__blank rel='noopener'}
afin d'utiliser les fonctions qui lui manquent sans pour autant pouvoir accéder
à leurs codes (d'autres versions du fichier sont disponibles, pour
[Python 3.8.10][A3810.pyc]{target=__blank rel='noopener'} et [Python 3.10.1][A3101.pyc]{target=__blank rel='noopener'}).
Pour cela, il suffira de faire :

    ``` python
    from A34 import *
    ```
    
:warning: **IMPORTANT** :warning:

Concernant la mécanique du jeu, vous ne serez pas trop de deux pour envisager
tout ce qu'il y a à prévoir. **Je vous demande *expressément* de passer par
une phase papier-crayon** ou chacun apporte sa contribution pour faire la
liste des différentes étapes, l'ordre dans lequel elles devront s'enchaîner
et les conditions d'arrêt du jeu. Vous pourrez décrire cette mécanique en
langage naturel (utilisez des « [listes à puces][liste-puces]{target='__blank' rel='noopener'} » et des
décalages pour rendre la structure de l'ensemble plus lisible), ou *écrire*
un algorithme, ou encore *dessiner* un [algorigramme][algorigramme]{target='__blank' rel='noopener'}.  
Quel que soit votre choix, vous **devrez** me soumettre le résultat de vos cogitations
avant que B ne se lance dans le codage...

??? info "Remarques"

    * Le fait que la fonction `affiche_console` soit mentionnée pour chacun
    des co-développeurs permet de mieux équilibrer la répartition des tâches :
    celui qui a fini en premier s'en occupera (si c'est B, de préférence *après*
    avoir recodé la fonction `analyse_saisie` ***sans**
    [regex][regex]{target=__blank rel='noopener'}* — plus
    de détails dans la [partie suivante](#validation)).

    * Naturellement, rien n'empêche une personne de tout faire toute seule (ne
    serait-ce que si votre groupe est d'effectif impair), mais j'insiste : vous
    **devrez** tout de même collaborer !

    * Afin de vous aider, et de comparer vos codes avec le résultat escompté,
    différentes versions « compilées » du jeu sont disponibles, pour
    [Python 3.4.2][demineur-console-342.pyc]{target=__blank rel='noopener'}, pour
    [Python 3.8.10][demineur-console-3810.pyc]{target=__blank rel='noopener'} et
    [Python 3.10.1][demineur-console-3101.pyc]{target=__blank rel='noopener'}.
    Je ne peux pas produire ces fichiers pour chacune des versions possibles,
    à vous de trouver l'exécutable Python ayant exactement la même version !



### B. Les contraintes à respecter

Voici les contraintes qui vous seront imposées : certaines viennent du contexte
(interaction à l'aide de commandes, depuis un terminal), d'autres uniquement
de moi 😉 !


#### 1. Paramètres du jeu

Le jeu devra pouvoir être paramétré : `largeur` et `hauteur` du plateau
seront définis _via_ des variables [éponymes](https://www.larousse.fr/dictionnaires/francais/%C3%A9ponyme/30582 "Si vous ne comprenez pas ce mot, cliquez pour en découvrir la définition"){target='__blank' rel='noopener'}.
Il en sera de même pour le pourcentage de mines, qui sera défini _via_ la
variable `max_mines` (sous forme d'un flottant). Le nombre total de mines
`nb_mines` sera alors calculé automatiquement.

 [//]: # (<a href="https://www.larousse.fr/dictionnaires/francais/%C3%A9ponyme/30582" title="Si vous ne comprenez pas ce mot, cliquez pour en découvrir la définition" target="_blank">éponymes</a>.)

??? info "Remarque"

    Reportez-vous à la [base de code](#a-lorganisation) fournie plus haut : 
    vous **devrez** respecter les noms des variables et des fonctions qui
    y figurent !


#### 2. Structure de données essentielles

Le `plateau` de jeu et l'`affichage` seront définis séparément, au travers
de deux variables éponymes, qui seront des **listes de listes** : étudiez
le code d'exemple, car il y a une subtilité : il faudra écrire `plateau[y][x]`
pour accéder au contenu de la case d'abscisse `x` et d'ordonnée `y` (en
effet, `plateau[y]` désignera une liste, qui elle-même désignera de façon
commode une ligne du terrain de jeu) ! Je vous *impose* donc d'écrire des
fonctions utilitaires afin de rendre les manipulations du plateau et de
l'affichage plus naturelles.

Le codage du `plateau` comme de l'`affichage` est également imposé (à
nouveau, reportez-vous au [code de base fourni plus haut](#a-lorganisation)).

??? example "Exemples"
    
    Pour `plateau`, un `0` désigne une case vide alors qu'un `1` désigne
    une case minée.
    
    Pour `affichage`, ce sera plus complexe, avec un dictionnaire permettant
    d'associer **plusieurs** caractères à un seul et même « statut ».
    En effet, en mode console, il sera utile de varier les affichages
    afin d'améliorer la lisibilité (voir la capture d'écran plus bas).

??? warning "Un détail qui a son importance"

    Vous aurez remarqué que les fonctions ne prennent **jamais** comme
    paramètre les variables `plateau` ou `affichage`, alors qu'elles
    parviennent très bien à en modifier les valeurs.
    
    C'est parce que ces variables sont des *objets Python* (la programmation
    orientée objets est au programme de terminale), que nous ne modifions
    pas directement depuis ces fonctions. Ce qu'on fait, à « l'intérieur »
    de ces fonctions, revient plutôt à demander à l'objet `plateau` (par
    exemple) de se *modifier lui-même*.
    
    Lors de l'exécution d'une de ces fonctions, `place_mine_au_hasard` par
    exemple, aucun paramètre ne vient fournir à la fonction les données
    dont elle a besoin pour travailler. En son sein, la variable `plateau`
    est inconnue. Python va alors chercher dans *l'espace de noms*-parent
    (celui du script tout entier) s'il trouve une variable ainsi nommée.
    C'est bien le cas ! En outre, cette variable est d'un type sophistiqué,
    elle a la capacité de modifier elle-même sa valeur, donc tout fonctionne.
    
    Saurez-vous alors expliquer pourquoi, contrairement à la fonction
    `place_mine_au_hasard` qui n'a aucun paramètre mais modifie tout de
    même la valeur de `plateau`, la fonction `modifier(tableau, x, y, valeur)`
    a un paramètre nommé `tableau` :wink: ?
    

#### 3. L'affichage

Par affichage, il faut entendre : le rendu, tel que vous le verrez, en tant
que développeur ou joueur.

Pour aider au repérage, vous **devrez**
afficher tout autour du terrain de jeu des guides. Vous procéderez comme
dans un tableur :

* horizontalement, des lettres majuscules viendront repérer
les colonnes ;
* verticalement, des nombres entiers viendront repérer les lignes.

Afin d'avoir une lisibilité optimale, les lettres apparaîtront au-dessus
**et** au-dessous du terrain de jeu, et ses lignes seront numérotées à
gauche **et** à droite. Voici une capture d'écran illustrant le résultat
attendu (sous Ubuntu, ici) :

![Démineur en mode console](images/rendu-console.png){width=600}


#### 5. La saisie

Qui dit jeu en mode console dit *interaction en mode console* avec le jeu ,
et donc saisie au clavier. Vous devrez faire en sorte que le joueur puisse
donner ses instructions au jeu conformément aux instructions lisibles sur
la capture d'écran précédente.

* Pour aller sur une case et révéler son contenu, le joueur devra saisir
les coordonnées de la case **suivies obligatoirement** d'un espace (pour
éviter les fausses manipulations :wink: !), Ainsi, les saisies « B13␣ »
ou « J0␣ » sont valides (le symbole « ␣ » est traditionnellement utilisé
pour aider à visualiser [une](https://fr.wikipedia.org/wiki/Espace_(typographie) "Hé oui, espace peut être féminin !"){target=__blank rel='noopener'} espace).  
Pour être précis, les coordonnées sont données sous la forme d'une lettre
(en premier) suivie de 1 à 2 chiffres (sans aucun guillemet, naturellement !).

* Remplacer l'espace par un « ! » marque la case comme étant potentiellement
minée.

* Remplacer l'espace par un « ? » marque la case comme étant incertaine
(l'objectif est d'aider le joueur à émettre des hypothèses en fournissant
un support visuel à ses suppositions).

* Remplacer l'espace par un « X » retire toute marque associée à la case.

<a name="validation"></a>Pour vous aider à valider les saisies de l'utilisateur,
une fonction toute prête vous est proposée : `analyse_saisie`. Elle fonctionne
grâce à une
**[expression rationnelle][regex]{target=__blank rel='noopener'}** :
cette notion *rigoureusement hors programme* facilitera bien les choses ici !
Pour utiliser cette fonction, reportez vous à sa docstring, dans le [code
source de base][base-code-demineur]{target=__blank rel='noopener'}. 

??? info "Remarque"

    La validation des saisies d'un utilisateur est une partie importante
    d'un programme prévu pour interagir avec des humains. Il est bien connu
    que ceux-ci sont maladroits, et parfois même malveillants.	Ainsi, il
    est essentiel d'analyser tout ce qu'un utilisateur saisit, afin de vérifier
    que tout est conforme aux attentes, à ce qui est permis et attendu par
    le logiciel : il en va de la sécurité de l'ordinateur, de celle de l'éventuel
    « système d'information » dans lequel s'insère le logiciel en question,
    ainsi que de la cohérence dudit système d'information (si différents
    opérateurs ont des pratiques de saisie différentes, des entrées théoriquement
    similaires pourront ne pas l'être ! d'où des erreurs imprévisibles...).

??? tip "Défi"

    Les plus rapides parmi vous pourront chercher à remplacer cette fonction
    `analyse_saisie` prédéfinie par une fonction de leur cru.  
    **Attention !** Hors de question de se contenter d'un simple `try ... except ...` :
    outre le fait que cette structure soit hors-programme,
    elle ne permet que de gérer les *exceptions* (les erreurs) ! Elle **ne**
    permettrait **pas** d'empêcher une [saisie non souhaitée mais qui ne
    provoquerait pas d'erreur](https://m.xkcd.com/327/ "Comment une « innocente » mère de famille efface la base de données de l'école de son fils !"){target=__blank rel='noopener'}...


#### 6. Les tests

À venir : vous aurez bientôt à votre disposition deux fichiers permettant
de valider certains points de vos codes.

Notamment :

* la fonction qui initialise le jeu place-t-elle bien le bon nombre de mines ?
* la fonction qui détermine les coordonnées des cases voisines fonctionne-t-elle
comme on le souhaite ? Il faut tester quelques exemples, y compris des cas
limites (case `A0` par exemple) ou pathologiques (case de coordonnées `(150,-10)`
par exemple) ;
* la fonction qui compte le nombre de mines dans le voisinage d'une case
donne-t-elle le résultat attendu (sur un plateau de test) ?
* etc.


#### 7. :warning: Pour aller plus loin : révéler toute les cases vides possibles :warning:

Si vous essayez la version *bytecode* (pour [Python 3.4.2][demineur-tk-342.pyc]{target=__blank rel='noopener'} ;
[Python 3.8.10][demineur-tk-3810.pyc]{target=__blank rel='noopener'} ; [Python 3.10.1][demineur-tk-3101.pyc]{target=__blank rel='noopener'}),
ou si vous essayez le jeu tel qu'on peut le trouver sur certains systèmes
(Windows® et GNU/Linux au moins), vous constaterez que les cases vides se
« propagent en cascade ». Les programmeurs de ces versions ont cherché à
simplifier la vie du joueur : toute case ayant 0 mine dans son voisinage
est nécessairement entourée de 8 cases elles-mêmes vides : pourquoi forcer
alors le joueur à cliquer sur chacune d'entre elles ?

Je vous propose d'essayer, vous aussi, de programmer cette simplification
du *gameplay*. Pour cela, on peut utiliser une [approche récursive][récursivité]{target=__blank rel='noopener'},
mais cette notion n'apparaît que dans le programme de la classe de terminale.

Je vous propose donc d'utiliser une liste Python nommée `cases_a_analyser`,
contenant initialement les coordonnées de la case que le joueur a décidé
de visiter. Petit à petit, cette liste se verra augmentée des coordonnées
de toute case voisine non encore visitée et qui n'aura pas de mine dans
son propre voisinage.

Il faudra bien sûr retirer de la liste au fur et à mesure les cases qui
auront été analysées (voir la méthode `pop` des listes).

??? info "Remarque"
    
    L'ordre dans lequel on traitera les cases n'a pas d'importance, ici.
    De même que celui dans lequel on les retire de la liste pour décoompte
    des mines alentours.
    
    En revanche, cela a parfois de l'importance, et conduit à des logiques
    de traitement particulières, dites « en file » ou « en pile ». En science
    informatique, ces logiques de traitement correspondent à deux types
    abstraits de données, qu'on appelle par extension « file » et « pile ».
    
    * **Dans une File** : le **PREMIER** élément qui entre est le **PREMIER**
    à en sortir (comme dans une *file d'attente* à une caisse, où le premier
    arrivé est le premier à être pris en charge — en anglais, le terme employé
    est d'ailleurs *queue*, qui a exactement ce sens).
    On parle également de structure FIFO, de l'anglais *First In First Out*.
    
    * **Dans une Pile** : le **DERNIER** élément qui entre est le **PREMIER**
    à en sortir (comme dans une *pile d'assiettes* ouù la dernière, posée
    au sommet de la pile, est la première qu'on attrappe ensuite).
    On parle également de structure LIFO, de l'anglais *Last In First Out*.




## Partie 2 : le jeu avec une interface graphique Tkinter

Pour faire cette partie, vous aurez dû vous familiariser d'abord avec Tkinter,
la bibliothèque, en étudiant au moins le contenu du premier fichier de
[cette archive (accès réservé)](https://e-education.recia.fr/moodle/mod/resource/view.php?id=1223931 "Python - Introduction à Tkinter et à la création de jeux vidéos Fichier"){target=__blank rel='noopener'}.

Vous consulterez avec profit la [documentation de Tkinter][tkinter-doc-fr]{target=__blank rel='noopener'},
traduite en français par les bons soins d'un collègue enseignant dans notre
académie : <http://tkinter.fdex.eu/>{target=__blank rel='noopener'}.

Voici quelques étapes que vous pourrez suivre pour adapter le code précédemment
mis au point pour lui ajouter une interface graphique.

### A. Concevoir l'interface graphique du jeu

On parle de GUI en anglais (pour *Graphical User Interface*). Je vous recommande
de rechercher la simplicité.

1. Créer une fenêtre.

2. Créer autant de [boutons cliquables][tkinter-button]{target=__blank rel='noopener'}
qu'il y a de cases. Ces boutons devront être positionnés à l'aide d'un [gestionnaire de positionnement « grid »](http://tkinter.fdex.eu/doc/gp.html "Tkinter : le geometry manager « grid »"){target=__blank rel='noopener'}
(*geometry manager* en anglais).

    ??? warning "Remarque **TRÈS** importante"
    
        Lorsqu'on demande à Tkinter de créer (en terminale on dira « instancier »)
        un widget, par exemple avec l'instruction `b = Button(f, text="!")`,
        la variable `b` désigne alors l'« objet bouton » créé, ce qui nous
        permet ensuite de modifier la configuration dudit bouton. Il est
        ainsi possible de changer son texte à l'aide de l'instruction
        `b["text"] = "?"`. Il est donc essentiel de mémoriser *chaque* bouton
        créé, afin de pouvoir faire évoluer son affichage en fonction des
        besoins. Vous aurez donc besoin d'une nouvelle liste de listes,
        que vous désignerez obligatoirement par la variable `boutons` (notez
        le pluriel).

3. Créer, *en dessous* de tous les boutons précédents, deux [labels](http://tkinter.fdex.eu/doc/labw.html "Tkinter : les labels"){target=__blank rel='noopener'}
pour y afficher le nombre total de mines et le nombre de mines que le joueur
pense avoir localisées.

4. Attacher à *chaque* bouton **deux** fonctions Python.
    - L'une sera chargée de traiter les [évènements][tkinter-event]{target=__blank rel='noopener'}
que Tkinter génèrera lors d'un **clic gauche** (un clic sur le bouton gauche
de la souris, ou du touchpad, ou...). Vous pourrez vous appuyer sur la fonction
`analyse_voisinage` déjà définie. L'objectif de cette fonction est de modifier
l'affichage en fonction des résultats de l'analyse :
        + soit la case est vide, auquel cas le bouton correspondant n'est
          plus cliquable, et son texte est soit vide, soit indique le nombres
          de mines à proximité ;
        + soit la case est minée, le jeu s'arrête et le plateau est révélé ;
        + naturellement, le jeu peut aussi être terminé de façon plus positive !
          Si toutes les cases vides ont été visitées et toutes les mines
          identifiées, c'est la victoire !
    - L'autre sera en charge de l'altération de l'affichage (pour marquer
    une case comme étant probablement minée, par exemple). Je vous suggère
    de nommer cette fonction `modifie_case` et de l'associer au **bouton
    droit** de la souris.
    - Ces fonctions auront **obligatoirement** un argument, nommé `evenement`.
    Cet [évènement Tkinter][tkinter-event]{target=__blank rel='noopener'}
    portera en lui de nombreuses informations utiles, notamment celles qui
    vous permettront de retrouver les coordonnées de la case qui lui correspond
    (*via* la variable `plateau`).


### B. Le fonctionnement du jeu en mode graphique

La programmation d'interface graphique ne repose pas sur une logique procédurale
du code, dans laquelle les instructions s'enchaînent les unes après les
autres. Elle relève de ce qu'on appelle la « programmation évènementielle ».
Dans cette logique, une boucle infinie est démarrée (la `mainloop` de Tkinter),
durant laquelle du temps est simplement gâché (passé à *ne rien faire !*),
en attendant une action de l'utilisateur.

Naturellement, le temps n'est pas perdu en totalité : très régulièrement
et très fréquemment, les « objets Tkinter » vérifient si une action n'a
pas été initiée par l'utilisateur (cliquer sur un bouton de la souris, presser
une touche du clavier, etc.). Si cette action, cet *évènement* correspond
à un évènement prévu car programmé, la fonction correspondante est alors
automatiquement appelée.

**Conséquences**

* **La boucle principale du jeu en version console, qui commence
par `while jeu_en_cours`, disparaît**, purement et simplement.

* **Plus besoin d'inviter le joueur à saisir ses instructions** :
l'interaction avec le jeu se fait directement à la souris. La fonction qui
analyse et vérifie la validité de la saisie devient également inutile.

* **Les modifications de l'affichage** sont des conséquences directes des
interactions du joueur avec le jeu, elles ont donc lieu dans les fonctions
attachées aux évènements « clic gauche » et « clic droit » de la souris.

* Les variables comptant le nombre de mines soupçonnées, et mémorisant l'état
du jeu (partie en cours, issue victorieuse ou non) devront, pour pouvoir
être modifiées depuis les fonctions appelées par les évènements associés
aux boutons, être rassemblées dans un objet (qui aura donc la capacité de
se modifier sur demande). J'ai choisi d'utiliser un dictionnaire nommé `statuts`
et dont la définition initiale sera :

    ``` python
    statuts = { "jeu en cours" : True, "perdu" : False, "mines soupçonnées" : 0 }
    ```

* Vous rencontrerez probablement une difficulté avec le changement d'état
des boutons. Cette difficulté est inhérente au fonctionnement de Tkinter.
Pour la contourner, un indice se trouve
[ici](https://stackoverflow.com/questions/63278527/python-tkinter-mouse-left-click-works-only-with-double-click){target=__blank rel='noopener'}.


### C. Pour aller plus loin

Améliorez l'esthétique ! Saviez-vous que les boutons peuvent accueillir
des images, par exemple ? Regardez [le paramètre `bitmap`][tkinter-bitmap]{target=__blank rel='noopener'}
du *widget* [Button][tkinter-button]{target=__blank rel='noopener'}. Il
vous faudra adapter un peu ce qui est indiqué dans ce dernier lien, car
le contexte décrit est celui d'un [`Canvas` Tkinter][tkinter-canvas]{target=__blank rel='noopener'},
qu'on n'utilise pas ici...


### D. Un aperçu du résultat que vous pourriez obtenir

Vous pouvez faire fonctionner le *bytecode* que j'ai généré à partir du programme
que j'ai réalisé moi-même. Vous devriez obtenir quelque chose qui ressemble
à la capture d'écran ci-dessous :

![Démineur en mode graphique](images/rendu-gui.png)

* *Bytecode* pour [Python 3.4.2][demineur-tk-342.pyc]{target=__blank rel='noopener'}.
* *Bytecode* pour [Python 3.8.10][demineur-tk-3810.pyc]{target=__blank rel='noopener'}.
* *Bytecode* pour [Python 3.9.2-3][demineur-tk-3923.pyc]{target=__blank rel='noopener'}.
* *Bytecode* pour [Python 3.10.2][demineur-tk-3101.pyc]{target=__blank rel='noopener'}.


## Partie 3 : pour aller encore plus loin

Si vous lisez avec attention la [page Wikipedia consacrée au démineur][demineur]{target=__blank rel='noopener'}, 
vous constaterez qu'il y a dans la [section « algorithmes »][demineur-algo]{target=__blank rel='noopener'}
quelques considérations intéressantes sur le temps nécessaire pour placer
des mines, ou la façon de s'y prendre (notamment pour rendre les choix du
joueur indépendants du hasard). Vous pourrez vous y intéresser...


[base-code-demineur]: codes/demineur-console-base.py "Code de base du démineur en mode console (incomplet)"
[base-code-A]: codes/A.py "Partie A du code de base du démineur en mode console (incomplet)"
[base-code-B]: codes/B.py "Partie A du code de base du démineur en mode console (incomplet)"
[A342.pyc]: codes/A342.pyc "Bytecode du fichier A (opérationnel, pour Python 3.4)"
[A3810.pyc]: codes/A3810.pyc "Bytecode du fichier A (opérationnel, pour Python 3.8)"
[A3101.pyc]: codes/A3101.pyc "Bytecode du fichier A (opérationnel, pour Python 3.10)"
[demineur-console-342.pyc]: codes/demineur_console_34.pyc "Bytecide du démineur en mode console (opérationnel, pour Python 3.4.2)"
[demineur-console-3810.pyc]: codes/demineur_console_3810.pyc "Bytecide du démineur en mode console (opérationnel, pour Python 3.8.10)"
[demineur-console-3101.pyc]: codes/demineur_console_3101.pyc "Bytecide du démineur en mode console (opérationnel, pour Python 3.10.1)"
[demineur-tk-342.pyc]: codes/demineur_tk_342.pyc "Bytecode du démineur (opérationnel, pour Python 3.4.2)"
[demineur-tk-3810.pyc]: codes/demineur_tk_3810.pyc "Bytecode du démineur (opérationnel, pour Python 3.8)"
[demineur-tk-3923.pyc]: codes/demineur_tk_3923.pyc "Bytecode du démineur (opérationnel, pour Python 3.9.2-3 de Debian 12)"
[demineur-tk-3101.pyc]: codes/demineur_tk_3101.pyc "Bytecode du démineur (opérationnel, pour Python 3.10)"
[tkinter-doc-fr]: http://tkinter.fdex.eu/ "Documentation de Tkinter (traduite en français)"
[tkinter-button]: http://tkinter.fdex.eu/doc/bw.html "Tkinter : classe Button"
[tkinter-event]: http://tkinter.fdex.eu/doc/event.html "Tkinter : les évènements"
[tkinter-bitmap]: http://tkinter.fdex.eu/doc/caw.html#can-bitmaps "Tkinter : les images"
[tkinter-canvas]: http://tkinter.fdex.eu/doc/caw.html "Tkinter : le canevas"
[récursivité]: https://fr.wikipedia.org/wiki/Algorithme_r%C3%A9cursif "Algorithme récursif"
[regex]: https://fr.wikipedia.org/wiki/Expression_régulière "Qu'est-ce qu'une expression rationnelle ?"
[algorigramme]: https://fr.wikipedia.org/wiki/Organigramme_de_programmation "Qu'est-ce qu'un algorigramme ?"
[liste-puces]: https://fr.wikipedia.org/wiki/Puce_(typographie) "Qu'est-ce qu'une liste à puces ?"
[demineur]: https://fr.wikipedia.org/wiki/D%C3%A9mineur_(genre_de_jeu_vid%C3%A9o) "Le démineur (genre de jeux vidéos)"
[demineur-algo]: https://fr.wikipedia.org/wiki/D%C3%A9mineur_(genre_de_jeu_vid%C3%A9o)#Algorithmes "Algorithmes de placement de mines pour le démineur"
