#!/usr/bin/env python3
# coding: utf8
from tkinter import *

touches = set()

def enfoncer(evt):
    touches.add(evt.keysym)

def relacher(evt):
    if evt.keysym in touches:
        touches.remove(evt.keysym)


def action():
    if "Left" in touches:
        c.move(r, -20, 0)
    if "Right" in touches:
        c.move(r, 20, 0)
    if "Up" in touches:
        c.move(r, 0, -20)
    if "Down" in touches:
        c.move(r, 0, 20)
    f.after(25, action)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(350, 350, 450, 450, fill="red", width=10, outline="blue")
c.pack()
action()
f.bind('<KeyPress>', enfoncer)
f.bind('<KeyRelease>', relacher)
f.mainloop()
