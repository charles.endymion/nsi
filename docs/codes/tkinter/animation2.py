#!/usr/bin/env python3
# coding: utf8
from tkinter import *
from random import randrange as rnd

# Dictionnaire rassemblant les paramètres essentiels du « jeu ».
config = {"Dx": rnd(1, 21), "Dy": rnd(1, 21), "animation": ""}

def bouge(event=None):
    """Fonction gérant les animations"""
    Dx = config["Dx"]
    Dy = config["Dy"]
    c.move(o, Dx, Dy)
    if c.coords(o)[0] <= 0 or c.coords(o)[2] >= Xmax:
        config["Dx"] = -Dx
    if c.coords(o)[1] <= 0 or c.coords(o)[3] >= Ymax:
        config["Dy"] = -Dy
    animation = f.after(20, bouge)              # Délai de 20 ms, soit 50 FPS...
    c.itemconfigure(t, text="Animation : {}".format(animation))
    no_animation = int(animation.split("#")[1]) # Extrait l'entier de 'after#123'.
    config["animation"] = animation

# Construction de l'interface graphique (minimaliste).
f = Tk()
f.title("Animations avec le canevas de Tkinter")
# Le canevas
c = Canvas(f, background="light yellow", width=800, height=600)
c.configure(borderwidth=5, relief=RIDGE, highlightthickness=2)
c.pack(padx=10, pady=10)
# Les éléments graphiques du canevas. Nouveauté : un texte !
o = c.create_oval(200, 50, 250, 100, fill="yellow")
t = c.create_text(10, 10, text="Animation : stoppée", anchor='nw')
# Un bouton
b = Button(f, text='Bouge de là !')
b.pack()
b.bind('<Button-1>', bouge)     # '<Button-1>' est le bouton gauche de la souris

c.update() # Indispensable, sinon Xmax et Ymax (ci-dessous) vaudront 1 chacun !
Xmax, Ymax = c.winfo_width(), c.winfo_height()

f.mainloop()    # Démarrage de la boucle principale de la fenêtre.
