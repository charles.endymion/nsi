# Python : traitements d'images simples et stéganographie

Dans ce document, vous allez découvrir les bases de la manipulation d'images
en Python, en écrivant des programmes qui réalisent des actions simples
sur les images :

* créer informatiquement des images simples ;
* conversion d'une image en niveaux de gris vers une image noir & blanc ;
* conversion d'une image en couleurs vers une image en niveau de gris ;
* [« inverser » une image](https://fr.wikipedia.org/wiki/Film_n%C3%A9gatif){target='__blank' rel='noopener'}
(par exemple, pour une image noir & blanc, changer tous les pixes noirs
en pixels blancs et réciproquement).

Vous verrez dans un second temps comment mettre en place un système de
stéganographie simple : **la stéganographie est l'art de la dissimulation**,
son objectif est de _cacher des informations_ aux personnes non averties
(alors que la _cryptographie_ cherche à rendre incompréhensibles (on dit
aussi -inintelligibles_)ces mêmes informations à toute personne non habilitée).
Typiquement, la stéganographie permet de _cacher une image **dans** une autre
image_.

Tout cela sera initialement réalisé avec des formats d'images très simples :
les « _[Portable Pixmaps ](https://fr.wikipedia.org/wiki/Portable_pixmap){target='__blank' rel='noopener'}_ »
(des formats plus standards, tels que le [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics){target='__blank' rel='noopener'},
seront ensuite utilisés).

Dans la suite, vous pourrez travailler à partir des images fournies dans
[l'archive associée à ce document](NSI-40-Images-et-stéganographie-Ressources.zip)
(ce devra **obligatoirement** être le cas lorsqu'un nom précis d'image vous
sera indiqué).


## Motivations

Ces travaux sur les images vont vous permettre :

* de réviser l'accès à des fichiers en lecture et en écriture ;

* de reparler de numérisation et de codage des informations visuelles (l'occasion de revoir la numération binaire), ainsi que la façon dont
on peut agir dessus ;

* de renforcer la maîtrise des boucles et des listes (ainsi que des listes
de listes, car les images sont essentiellement des tableaux de pixels à 2 dimensions) ;

* d'avoir une première approche de la compression de données. En effet,
une image de grande taille (4000 × 3000 pixels par exemple), enregistrée au format PPM (présenté plus bas), occupe une place énorme (environ 90 Mo pour une image PPM ASCII, dans les 40 Mo pour une image PPM binaire) ! Sur des images « simples » (au sens « pas trop riches en détails »), on verra l'effet du codage [RLE](https://fr.wikipedia.org/wiki/Run-length_encoding){target='__blank' rel='noopener'} (en guise de hors d'œuvre, car en Terminale on découvrira le [codage de HUFFMAN](https://fr.wikipedia.org/wiki/Codage_de_Huffman){target='__blank' rel='noopener'}, plus sophistiqué — mais dont l'implémentation est bien plus délicate) ;

* d'acquérir des notions élémentaires sur la sécurité. En effet, rien de tel qu'un message incompréhensible pour attirer l'attention et aiguiser l'intérêt,
au contraire d'une $n$-ième image échangée, en toute transparence, _via_ un réseau quelconque (MMS, application dédiée, site web communautaire, ou même au sein d'un antique courriel)... C'est là tout l'intérêt de la stéganographie, qui recherche la sécurité dans la lumière, au contraire du chiffrement, qui recherche la sécurité par l'ombre et l'obfuscation[^stéganographie-vs-cryptographie].

Sachez en outre :

* que la stéganographie joue un rôle essentiel dans l'industrie du divertissement.
Elle sert en effet à [« tatouer » des œuvres numériques](https://fr.wikipedia.org/wiki/Tatouage_numérique){target='__blank' rel='noopener'}
(en anglais on parle de _« watermarking »_, terme qui signifie filigrane).
L'objectif du tatouage est de [marquer de façon indélébile un fichier](https://interstices.info/le-tatouage-de-son/){target='__blank' rel='noopener'},
afin par exemple d'assurer la protection des droits d'auteur. Les technologies
associées sont commercialisées par divers laboratoires ou entreprises, et
employées par les plates-formes de vente en ligne de musiques et et de vidéos
dématérialisées afin de lutter contre le piratage[^tatouage-commerce] ;

* que cette logique de marquage ineffaçable peut également être utilisée
à des fins qui peuvent parfois être plus sensibles (lutte contre l'espionnage)...
ou contestables (comme par exemple traquer les lanceurs d'alertes)[^tatouage-traitres].

!!! note "À faire vous-même"

    === "Vos objectifs"

        1. Trouver des techniques non (purement) informatiques de stéganographie.
        Soyez inventifs, voire artistes !
        
        2. La lutte contre le piratage est un sujet complexe et délicat.
        Il importe de s'informer sur les aspects légaux lié aux œuvres numériques,
        les moyens techniques (tels que les [DRM](https://fr.wikipedia.org/wiki/Gestion_des_droits_numériques){target='__blank' rel='noopener'})
        et juridiques de les protéger. La question des licences, précisant
        ce que l'on peut faire d'une œuvre numérique, est alors centrale.
        Il faut aussi savoir qu'en FRANCE existe une [exception juridique permettant la copie d'une œuvre en vue d'un usage privé](https://fr.wikipedia.org/wiki/Copie_priv%C3%A9e#France){target='__blank' rel='noopener'}...
        mais qu'[HADOPI](https://fr.wikipedia.org/wiki/Haute_Autorité_pour_la_diffusion_des_œuvres_et_la_protection_des_droits_sur_internet){target='__blank' rel='noopener'}, une structure visant à la protection des intérêts
        économiques liés aux œuvres numériques, existe toujours.
        
        3. Le simple fait de travailler sur des images oblige donc à se
        poser la question de la légalité de leur obtention et des possibilités qui nous sont (ou pas) accordées de les modifier : il existe des banques d'images libres de droit (sous [licence Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR){target='__blank' rel='noopener'}), et même (au moins) un moteur
        de recherche d'images libres : trouvez-les !
        
    === "Indications"

        * Vous pourrez chercher du côté de la littérature, spécialement
        de certains poèmes qui sont plus que ce qu'ils paraissent être
        (par exemple quand on ne conserve que la première lettre de chaque
        vers, ou le premier mot, ou...). Certains sont assez fameux et...
        coquins :sweat_smile: !
        * Du côté des arts graphiques, certains dessins peuvent être perçus
        de deux façons différentes : on est dans le domaine des illusions
        d'optique...
        
        N'hésitez pas à parler de ce sujet avec les enseignants des disciplines
        concernées !

    === "Réponses"
        
        * [Les acrostiches sur Wikipedia](https://fr.wikipedia.org/wiki/Acrostiche#Une_rosserie){target='__blank' rel='noopener'}.
        
        * De nombreux dessins peuvent être perçus (plus ou moins facilement) de plusieurs façons.
        
            * Le [canard-lapin](https://fr.wikipedia.org/wiki/Canard-lapin){target='__blank' rel='noopener'}, peut-être la plus célèbre [image ambiguë](https://fr.wikipedia.org/wiki/Image_ambigüe){target='__blank' rel='noopener'}.
        
            * Verrez-vous [un joueur de saxophone ou une visage de femme](https://tra.img.pmdstatic.net/scale/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcam.2F2020.2F02.2F05.2Ff77b17f6-fcba-449f-92a4-aa6da2cb5a09.2Ejpeg/autox600/quality/65/un-visage-ou-un-joueur-de-saxophone.jpg){target='__blank' rel='noopener'} ?
            * [Ne percevrez-vous qu'un arbre, ou des visages](https://tra.img.pmdstatic.net/scale/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcam.2F2020.2F02.2F05.2Fa926f918-e868-4fcd-8152-41d44c67794f.2Ejpeg/autox600/quality/65/voici-l-arbre-a-visages.jpg){target='__blank' rel='noopener'} (et dans ce cas, combien) ?
            * [Voyez ici](https://i.pinimg.com/236x/23/29/3d/23293dff61a412d204b4ed4d2454831a--sleeping-tiger-tiger-art.jpg){target='__blank' rel='noopener'} un paysage qui comporte un tigre... un seul ?
            * Verrez-vous dans [ce tableau](https://i.pinimg.com/236x/c5/24/f9/c524f99c508f247c5652d60e775b97d4--oleg-shuplyak-old-mans.jpg){target='__blank' rel='noopener'} un paysage champêtre ou le visage d'un vieux moustachu ?
            * [Dernier exemple](https://i.pinimg.com/236x/a0/b2/df/a0b2df13702273b399b8e25dde68fc44--oleg-shuplyak-amazing-art.jpg){target='__blank' rel='noopener'} : discernerez-vous un artiste-peintre ou un visage ?
            
        * [Openverse](https://wordpress.org/openverse/){target='__blank' rel='noopener'} (anciennement [Creative Commons Search](https://oldsearch.creativecommons.org/){target='__blank' rel='noopener'} est un moteur de recherche spécialisé dans les médias
        diffusés sous [licences Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR){target='__blank' rel='noopener'} ou relevant du [domaine public](https://fr.wikipedia.org/wiki/Domaine_public_(propriété_intellectuelle)){target='__blank' rel='noopener'}.



## Découverte des formats PPM

### Description

Les formats d'images [Portable Pixmaps](https://fr.wikipedia.org/wiki/Portable_pixmap){target='__blank' rel='noopener'} regroupent en fait 3 formats différents de fichiers graphiques,
prévus pour pouvoir être utilisés sans difficulté pour les échanges _via_ la messagerie électronique qui, à ses débuts, n'acceptait que du texte encodé en [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'}.

Ce sont des formats d'_images **matricielles**_ (les pixels de l'image sont représentés par un tableau de valeurs, qu'on appelle également matrice en sciences d'où la dénomination « d'images matricielles »). Leur organisation interne est très simple, ce qui la rend facilement lisible (et donc intelligible) _par un humain !_ **Vous pourrez ainsi aisément observer l'impact de vos programmes** (qui transformeront les images) **sur l'organisation interne des fichiers associés,** chose impossible avec les formats classiquement utilisés, plus modernes et sophistiqués.

**Il vous suffira d'ouvrir les fichiers-images directement dans un simple éditeur de textes (Notepad, Gedit, ...).** On évite ainsi la « magie » liée aux bibliothèques telles que [Pillow](https://pillow.readthedocs.io/){target='__blank' rel='noopener'}[^Pillow].

* Le [Portable BitMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PBM){target='__blank' rel='noopener'} ou **PBM** est utilisé pour des images noir et blanc : un pixel noir est représenté par un caractère 1, un pixel blanc est codé par un caractère 0.

* Le [Portable GrayMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PGM){target='__blank' rel='noopener'} ou **PGM** est utilisé pour des images en nuances de gris (on dit « niveaux de gris »). Une valeur maximale est définie comme limite aux valeurs codant la profondeur de gris : elle est associée au blanc (absence de gris). Chaque niveau de gris est codé par une valeur entre 0 et cette valeur maximale, proportionnellement à son intensité (un pixel noir, donc _dépourvu de blanc_, correspond donc à la valeur 0).

* Le [Portable PixMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PPM){target='__blank' rel='noopener'} ou PPM est utilisé pour des images en couleurs. Chaque pixel est codé par trois valeurs (définissant le niveau de rouge, de vert et de bleu du pixel). Une valeur maximale est également utilisée pour limiter les niveaux de couleur. 

Ces 3 formats ont été définis dans le cadre du projet [NetPBM](http://netpbm.sourceforge.net/){target='__blank' rel='noopener'}. 


### Convertir une image en PxM

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        1. Prendre une photographie avec votre téléphone portable — attention à bien respecter le [droit à l'image et au respect de la vie privée](https://www.service-public.fr/particuliers/vosdroits/F32103#:~:text=Le%20droit%20%C3%A0%20l'image,vous%20pouvez%20saisir%20le%20juge.){target='__blank' rel='noopener'} !
        2. Convertir cette photographie (probablement enregistrée sur votre
        téléphone au format [JPEG](https://fr.wikipedia.org/wiki/JPEG){target='__blank' rel='noopener'} ou [HEIF](https://fr.wikipedia.org/wiki/High_Efficiency_Image_File_Format){target='__blank' rel='noopener'} si vous utilisez un appareil Apple®), en fichier PPM.
        3. Comparez les tailles des fichiers JPEG et PPM. Que peut-on en conclure ?
        4. Trouvez un logiciel de compression de données libre et gratuit, et utilisez-le pour compresser l'image PPM (par exemple aux formats ZIP et 7z). Comparez à nouveau les tailles du fichier JPEG et du fichier PPM compressé. Qu'en conclure ?
        5. Pour aller plus loin :
            * jouez avec le niveau de compression proposé lors de l'enregistrement d'images en JPEG et comparez à la fois la taille des fichiers obtenus et le rendu à l'écran d'images compressées avec un taux de 95 %, 80 %, 50 % et 20 % (ce paramètre peut aussi être appelé « qualité », en fonction du logiciel).  
            :warning: **Attention** à bien créer un nouveau fichier à chaque fois, de sorte à **ne pas** effacer le fichier d'origine !
            * comparez à nouveau taille et qualité avec la même image enregistrée au format [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics){target='__blank' rel='noopener'} ;
            * que donne la compression au format ZIP d'une image JPEG ?
        
    === "Indications"

        * Vous pourrez utiliser [GIMP][gimp]{target='__blank' rel='noopener'} pour changer de type de fichiers, un logiciel de traitement d'images *libre*, *gratuit* et fonctionnant sur une grande variété de systèmes d'exploitation différents.
        * Un excellent logiciel de compression-décompression, également libre et gratuit, se nomme [7-zip](https://www.7-zip.org/download.html){target='__blank' rel='noopener'}.
    
    === "Éléments de réponses"
    
        * Les formats JPEG / HEIF donnent des fichiers de bien plus petite taille (en octets) que le format PPM. Ils ont été conçus pour cela ! Mais cela a un coût : plus le niveau de qualité baisse (donc plus le taux de compression augmente), plus la qualité de l'image se dégrade : on parle de **compression avec pertes**. L'enregistrement d'une image dans ces formats supprime des informations, c'est pour cela que la taille du fichier obtenu diminue. Mais si l'on en supprime trop, cela finit par se voir...
        * Le format PNG permet d'enregistrer une image _sans perte d'information_, donc en conservant _tous_ les détails de l'image d'origine. Il fait appel à un algorithme de compression de données, mais comme il y a toujours autant d'information à enregistrer, la taille du fichier obtenu ne diminue pas autant que lorsqu'on utilise le format JPEG ou HEIF.
        * Parfois, compresser deux fois des fichiers permet de gagner encore un peu en taille, mais le gain est souvent très limité. Dans certains cas, le fichier ainsi obtenu peut même être _plus volumineux_ que le fichier d'origine !

### Description des formats PxM 

Les 3 formats (noir et blanc, niveaux de gris, couleurs) existent chacun en deux variantes : binaire ou [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'}. **On ne s'intéressera dans la suite qu'aux fichiers [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'} !**

Les fichiers PxM (c'est-à-dire PBM, PGM ou PPM) ont une structure similaire. On trouve, dans l'ordre :

1. le _nombre magique_ du format (codé sur 2 octets) : il indique le type de format (PBM, PGM, ou PPM) et la variante (binaire ou ASCII) suivi d'un caractère d'espacement (comprendre : l'espace, la tabulation ou le caractères de « nouvelle ligne »). Les différents types existants sont :

<center>

| ASCII          | PBM | PGM | PPM |
|:--------------:|:---:|:---:|:---:|
| Nombre magique | P1  | P2  | P3  |


| Binaire        | PBM | PGM | PPM |
|:--------------:|:---:|:---:|:---:|
| Nombre magique | P4  | P5  | P6  |

</center>


2. la _largeur_ de l'image (nombre de pixels, écrit explicitement sous forme d'un nombre en caractères ASCII) suivie d'un caractère d'espacement (même remarque que précédemment), puis de la _hauteur_ de l'image (codée de la même façon) à son tour suivie d'un caractère d'espacement ;

3. pour les images PGM et PPM, la valeur maximale du codage d'une (composante de) couleur est ensuite indiquée (typiquement 255, soit 256 nuances — mais ce nombre peut atteindre 65$\,$535, sans l'excéder). Comme d'habitude, ce nombre est suivi d'un caractère d'espacement ;

4. les _données_ de l'image : succession des valeurs associées à chaque pixel. L'image est codée _ligne par ligne_ en partant du haut, chaque ligne étant codée de gauche à droite.  
En PBM, il est inutile (mais pas impossible) de séparer les valeurs par un caractère d'espacement, mais c'est obligatoire pour les PGM et PPM.  
Pour les images PPM, chaque pixel est codé par un triplet de valeurs (une pour coder la composante rouge, une pour la composante verte, une pour la composante bleue), toujours séparées par un caractère d'espacement.

??? info "Remarque"

    Toutes les lignes commençant par le caractère # sont ignorées (ce sont des lignes de _commentaires_). Chaque ligne ne devrait pas dépasser 70 caractères, mais en pratique cela ne semble pas poser de problème...
        

### Trois exemples

#### Un smiley en PBM

![](images/40/smiley.png){ align=right width=300 }

``` title="Code de l'image « smiley.pbm »"
--8<-- "docs/images/40/reformatage-smiley.pbm"
```

??? info "Remarque"

    Vous pouvez créer un nouveau fichier **texte**, copier-coller le texte ci-dessus dedans, puis renommer ce fichier en « `smiley.pbm` » (attention aux [extensions de fichier, souvent masquées par défaut sous Windows](https://www.commentcamarche.net/informatique/windows/185-afficher-les-extensions-et-les-fichiers-caches-sous-windows/#){target='__blank' rel='noopener'} !).
    
    Une fois cela fait, vous pourrez voir cette image dans une visionneuse supportant ce format (c'est par exemple le cas d'[IrfanView](https://www.irfanview.com/){target='__blank' rel='noopener'} sous Windows, qui est un logiciel gratuit pour un usage personnel, mais qui n'est pas _libre_). Pour la modifier, là encore il faut un logiciel de traitement d'images compatible, comme par exemple [GIMP][gimp]{target='__blank' rel='noopener'}.

[//]: # (This may be the most platform independent comment)


#### Un immeuble en PGM

![](images/40/immeuble.png){ align=right width=300 }

``` title="Code de l'image « immeuble.pgm »" hl_lines="3"
--8<-- "docs/images/40/reformatage-immeuble.pgm"
```    

??? info "Remarque"

    La valeur **0** code un pixel **noir** et la valeur **255** code un pixel **blanc** (cela peut paraître contre-intuitif au début !). Il faut comprendre que c'est en fait la quantité de blanc qui est ainsi codifiée : 0 signifie pas de blanc, ==255== (avec une valeur maximale ici définie à 255, cf. ligne 3 du code de l'image) correspond au maximum de blanc possible (donc le pixel est _totalement_ blanc).


#### Le drapeau français en PPM

![](images/40/drapeau-francais.png){ align=right width=300 }

``` title="Code de l'image « drapeau-français.ppm »" hl_lines="3"
--8<-- "docs/images/40/reformatage-drapeau-français.ppm"
```    

??? info "Explications"

    Cette image a une [définition](https://fr.wikipedia.org/wiki/Image_num%C3%A9rique#D%C3%A9finition_et_r%C3%A9solution){target='__blank' rel='noopener'}
    de 6 × 3 = 18 pixels. Chaque pixel voit sa couleur codée par 3 nombres :
    le 1^er^ pour le niveau de rouge, le 2^nd^ donne le niveau de vert, le
    ^3ème^ code pour le niveau de bleu. Les deux premières colonnes de pixels,
    de couleur bleue, correspondent donc à deux successions des nombres
    63, 63 et 255 : le triplet $(63, 63, 255)$ code pour la couleur bleue
    que vous pouvez observer ci-dessus.


### À vous !

À l'aide d'un éditeur de textes de votre choix, créez une image simple, d'une taille maximale de 32 × 32 pixels. Suggestions : une maison, une voiture, un livre, une fleur, un motif géométrique...


## Manipulation d'images

Important : détails d'implémentation

Les programmes que vous allez créer exploiteront une particularité intéressante de Je vous recommande l'emploi de cet excellent logiciel libre et gratuit pour convertir des images en . : chaque fichier PGM ou PPM qu'il crée contient une valeur et seulement une par ligne. De ce fait, parcourir l'image revient à lire le fichier ligne à ligne : c'est facile (voir la section [commandes:base]commandes Python de base) !


    Cela implique en revanche que vous ayiez créé soigneusement vos images avec ([produire:PPM]la marche à suivre est donnée en annexe) :
        
        [] aucun souci pour les images en niveaux de gris, comme par exemple python"ImageConteneur.pgm", fournie dans l'archive associée à ce document ;        [] en revanche, les images en noir  blanc devront être enregistrée au format PGM, pourtant prévu à la base pour des images en niveau de gris.  Ouvrez dans un éditeur de textes le fichier nommé python"ImageAcacher.pgm" (également fourni dans l'archive) : vous verrez que les pixels sont codés à l'aide de 2 valeurs différentes, 0 pour un pixel noir et 255 pour un pixel blanc (oui, c'est contre-intuitif).
        
    

Commandes Python de base

Vous aurez besoin de savoir manipuler les fichiers . Voici les commandes les plus utiles. Pour commencer, voyons comment ouvrir proprement une image :

python
with open("Image.pgm", "r") as image :
    # Faire quelque chose...
    # Attention au décalage des lignes qui suivent : il est impératif !

L'avantage de cette syntaxe est de ne pas avoir à se préoccuper de fermer le fichier après avoir travaillé dessus (oublier de le faire peut conduire à des incohérences dans son contenu lorsque le fichier est ouvert en écriture(Les données qu'on demande à un programme d'écrire dans un fichier ne sont pas forcément écrites lorsque la commande se termine : elles ont pu être mises en https://fr.wikipedia.org/wiki/MC3A9moiretamponmémoire tampon, en attente d'un moment favorable pour leur écriture effective. Si pendant cette attente le fichier venait à être refermé, ces données seraient perdues...)). On peut aussi travailler simultanément avec plusieurs fichiers :

python
with open("a.pnm", "r") as a, open("b.pnm", "r") as b, open("c.pnm", "w") as c :
    # Notez le "w", qui permet d'ouvrir un fichier en écriture...
    # Note : la commande "with ..." doit ABSOLUMENT tenir sur une seule ligne !

Une fois le fichier ouvert, on doit le lire ligne à ligne. Il faudra passer les premières lignes, non sans récupérer les informations indispensables : largeur et hauteur de l'image(Gimp introduit systématiquement une ligne de commentaires, qu'on peut décider de supprimer manuellement en modifiant le fichier à la main, avec un éditeur de textes. Il faut absolument éviter le bloc-notes de Windows ! est un excellent choix : c'est un éditeur de code - donc de texte - très connu, libre et gratuit.).

Après, une simple boucle infinie peut convenir : il faudra la quitter dès lors que l'on rencontrera une ligne vide, ce sera le signe que l'on a atteint la fin du fichier. Cela donnerait en Python quelque chose comme :

python
while True :
    ligne = c.readline()
    if ligne == " :
        break

Il faudra ensuite travailler sur les données, et les écrire ensuite dans le fichier destination. Il faut garder à l'esprit que les fichiers PGM et PBM produits par stockent chaque valeur sur une ligne indépendante, ce qui implique que chaque chaine obtenue par la commande pythonreadline() se terminera par un https://fr.wikipedia.org/wiki/Sautdelignecaractère de saut de ligne python"" : il faudra en tenir compte ! De meme, toute ligne écrite dans le fichier de destination devra se terminer par un caractère de passage à la ligne python"" :

python
conteneur.write("P2")
# Ou bien (on suppose que la variable "valeur" contient une chaîne de caractères)
conteneur.write(valeur+"")

Si nécessaire, éradiquer certains caractères indésirables en fin (ou en début de ligne) se fait à l'aide de la méthode strip() des objets chaînes de caractères :

python
s="   toto "
s.strip()   # Renvoie juste 'toto'

Enfin, je vous rappelle les commandes de base de Python pour convertir des chaînes de caractères en nombres et réciproquement :

python
int("238")   # Donne l'entier 238
str(48)      # Retourne la chaîne de caractères "48"
int("238") # Retourne l'entier 238 : Python simplifier toujours la vie !


Conversion d'une image en niveaux de gris en une image en noir  blanc

On travaillera à partir d'une image PGM (donc en niveaux de gris) générée par . Sur chaque ligne, à partir de la 4, on trouvera donc une valeur comprise entre 0 (le pixel est noir) et 255 (pixel blanc, la valeur 255 est celle qu'on rencontre le plus souvent). Le but du jeu est de réduire les 256 nuances, et de passer de 256 valeurs différentes possibles à 2 seulement : 0 ou 1(Au lieu de travailler avec une image de destination PBM en noir  blanc, on pourrait rester avec une image PGM en niveaux de gris consituée uniquement des valeurs 0 ou 1 255..).

On décide dans un premier temps d'adopter la logique suivante : les valeurs comprises entre 0 et 127 (bornes inclues) seront transformées en 1(Rappel : 0 est la valeur qui code un pixel noir pour une image PGM (niveaux de gris), alors que la valeur qui code le noir pour une image PBM est 1.), celles restantes (entre 128 et 255 donc) seront transformées en 0. Voici le code du programme pythonconversionNB.py, implémentant cette logique :

[linenos]pythonconversionNB.py

Votre mission : créer au début du programme une variable nommée SEUIL, grâce à laquelle on n'aura pas à aller fouiller dans le code à la recherche de la ligne qui convient afin de changer la valeur 127. Essayez ensuite différentes valeurs, entre 0 et 255, et observez l'effet des changements.


Inversion des nuances

En vous inspirant du programme précédent, réalisez :

    un programme qui, partant d'une image PBM (en noir  blanc), transforme chaque pixel blanc en pixel noir et vice-versa et enregistre le résultat dans une nouvelle image PBM ;
    un programme qui, partant d'une image PGM (en niveaux de gris), réalise une action similaire (à vous de définir ce que similaire peut signifier !) et enregistre le résultat dans une nouvelle image PGM.


Stéganographie

Le principe

Nous allons écrire un programme qui permettra de cacher l'image PBM (en noir  blanc) nommée python"ImageAcacher.pgm" à l'intérieur de celle en niveaux de gris appelée python"ImageConteneur.pgm"(Une image en noir  blanc enregistrée en PGM au lieu de PBM ? Relisez la section [détails]détails d'implémentation.). Pour simplifier l'écriture du programme, les deux images devront avoir rigoureusement la même taille (pour conserver des programmes simples, on partira du principe que ce sera toujours le cas(Si ce n'est pas le cas, utilisez pour redimensionner vos images : menu [,]Image, Échelle et taille de l'image...)). Le principe est le suivant :

    [] on parcourra successivement et simultanément tous les pixels de chacune des deux images afin de les combiner. On notera  la valeur codant un pixel de l'image conteneur (un entier compris entre 0 et 255),  son équivalent de l'image secrète et à camoufler (un entier qui vaudra 0 ou 255, valeur qu'il faudra ramener à 1 ou 0(Voir la section [détails]Important : détails d'implémentation.)), et enfin  la valeur obtenue par combinaison des deux valeurs précédentes ;
    [] pour chaque pixel situé au même emplacement dans les deux images, nous allons :
        
            supprimer le chiffre des unités de , ce qui en Python peut se faire à l'aide des commandes pythonvn = vc - vc 
                (On peut aussi travailler sur les chaînes de caractères, c'est pertinent vu le contexte (c'est d'ailleurs mon choix dans la [code:cacher]correction que je vous proposerai). En effet, on obtient des chaîne de caractères en lisant un fichier ASCII ! On pourrait alors écrire quelque chose comme pythonvn = vc[:-2] + vs (le python-2 sera indispensable car en pratique, chaque valeur sera enregistrée sur une ligne indépendante du fichier image généré par , et il faudra composer avec le https://fr.wikipedia.org/wiki/Findelignecaractère de fin de ligne "python" : fort heureusement, https://docs.python.org/3/library/functions.htmlopenPython est capable de gérer les https://fr.wikipedia.org/wiki/Findelignedifférentes façon de passer à la ligne qui existent selon les systèmes d'exploitation). Attention toutefois : pythonvn = vc[:2] + vs ne serait pas adapté : pourquoi ?
 Question subsidiaire : quelle approche sera la plus performante ? En d'autres termes, vaut-il mieux convertir les chaînes en entiers, travailler à la façon du mathématicien, et convertir à nouveau les résultats en chaînes, ou au contraire est-il plus efficient de se cantonner à l'astuce de l'informaticien ? À vous de chercher la réponse... si vous en avez le temps !) (tâchez de comprendre ces formules) ;

            ajouter  à  pour obtenir la valeur qui permettra de coder petit à petit l'image de destination, camouflant l'image à cacher : pythonvn = vn + vs ;
            
            écrire au fur et à mesure les nouvelles valeurs dans le fichier "ImageCachantUneImage.pgm" ;
            [] le décodage se fait de façon symétrique. À partir d'une image qui en camoufle une autre, il suffit d'extraire le chiffre des unités pour chaque valeur codant un pixel : pythonvs = vn 



La pratique : le camouflage

En vous inspirant des programmes précédents, écrivez un programme mettant en œuvre le principe qui vient d'être exposé.

Vous exécuterez 2 fois ce programme :

    [] avec l'image conteneur python"ImageConteneur.pgm" et l'image à cacher python"ImageAcacher.pgm". L'image produite devra s'appeler python"ImageCachantUneImage.pgm" ;
    
    [] puis avec l'image conteneur python"ImageConteneur2.pgm", l'image à cacher étant la même que précédemment. Cette fois, l'image produite devra s'appeler python"ImageCachantUneImage2.pgm".


Dans ces 2 situations, comparez l'image conteneur d'origine et le résultat de l'enfouissement de l'image secrète : qu'observez-vous ? Qu'en conclure ? Efforcez-vous de formuler une hypothèse quant à l'origine de ce problème ! Pouvez-vous imaginer une approche qui donne un meilleur résultat ?






















































Révéler une image camouflée : programme [code:reveler]reveler.py

C'est plus simple ! Il faut :

    [] ouvrir le fichier python"ImageCachantUneImage.pgm" en lecture et un fichier python"ImageRevelee.pbm" en écriture (on y inscrira les données dé-cachées au fur et à mesure). Notez l'extension python".pbm" de ce fichier : pour plus de simplicité, on décidera de créer un fichier PBM(En effet, on a tronqué au paragraphe précédent les valeurs de l'image source à la dizaine, puis remplacé le chiffre des unités par 0 ou 1. Par commodité, on partait d'une image 1 enregistrée par au format PGM, ce qui permettait d'avoir chaque valeur de pixel sur une ligne. Rien ne nous oblige à revenir à cette situation : autant conserver le chiffre des unités de l'image camouflante et le recopier dans une image PBM où chaque 0 ou 1 sera isolé sur une ligne...) ;
    [] écrire dans python"ImageRevelee.pbm" le type P1 et la taille de l'image (on n'aura pas besoin de la valeur qui gouverne la richesse en nuances(Il faudra donc sauter cette ligne, nécessairement présente dans l'image de départ !)) ;
    [] puis de chaque ligne de l'image source, il faudra extraire l'information utile et l'écrire dans l'image de destination.

C'est à vous !


Aller plus loin : on passe au binaire... et on utilise PIL !

C'est le moment où je vous fais observer, si vous ne l'avez pas encore remarqué, que cette approche manque de nuances (regardez ce que donnent les deux essais de camouflage de la même image secrète dans les deux images conteneur différentes que je vous propose) ! C'est lié au fait qu'en remplaçant le chiffre des unités dans les valeurs codant les pixels de l'image d'origine par 1 ou 0, on perd jusqu'à 10 nuances de gris : c'est une altération importante de l'image d'origine, et notre œil la remarque.

Il est dommage de perdre autant de nuances pour seulement stocker un 0 ou un 1 ! Pour agir plus discrètement, on peut convertir chaque valeur décimal en binaire, et mettre en œuvre la même logique mais uniquement sur le de poids faible : c'est le le plus à droite dans l'écriture binaire d'un nombre, celui qui détermine si un nombre est pair ou impair. En le remplaçant par la valeur 0 ou 1 issue de l'image à cacher, on ne perd plus qu'une seule nuance au plus(Avec cette approche, on peut également consacrer plus d'un de l'image d'origine au camouflage, ce qui permet de stocker soit une image avec quelques nuances de gris dans une image ayant plus de nuances de gris, soit de stocker plusieurs images en noir  bland au sein d'une même image conteneur.)!

On peut recycler le code existant en l'adaptant, naturellement. Mais je vous propose d'en profiter pour mettre en uvre des méthodes plus efficaces de manipulation des images, en nous appuyant sur la bibliothèque PIL(En fait, c'est https://python-pillow.org/Pillow que vous utiliserez, car PIL n'est plus maintenu ni mis à jour, et n'existe même pas pour Python 3.x ! [installer:pillow]Voir en annexe comment l'installer sous Windows dans un lycée de l'académie d'Orléans-Tours.), et d'employer des formats d'images plus traditionnels. Attention : vous devrez probablement [installer:pillow]installer PIL par vous-même !

Voici le code du programme pythoncacherMieux.py qui exploite cette approche plus fine : [linenos]pythoncacherMieux.py



Votre objectif : écrire le code du programme pythonrevelerMieux.py, qui révèle l'image cachée par le programme précédent.




Aller plus loin : niveaux de gris cachés dans la couleur

On peut imaginer cacher une image en niveau de gris au sein d'une image en couleurs : il suffit de cacher séparément la valeur des centaines, des dizaines et des unités dans l'une des composantes de l'image en couleurs. Par exemple :

    [] le chiffre des  centaines de l'image à cacher viendra écraser le chiffre des unités de la composante rouge de l'image-conteneur ;
    [] le chiffre des dizaines de l'image à cacher viendra écraser le chiffre des unités de la composante verte de l'image-conteneur ;
    [] le chiffre des unités de l'image à cacher viendra écraser le chiffre des unités de la composante bleue de l'image-conteneur.


Annexes

Produire et visualiser des images PBM / PGM / PPM

Vous pouvez utiliser https://www.gimp.org/Gimp pour convertir vos images :

    [] pour convertir une image en niveaux de gris : menu [,]Image, Mode, Niveaux de gris ;
    [] pour convertir une image en noir et blanc : menu [,]Image, Mode, Couleurs indexées puis choisir l'option Utiliser la palette noir  blanc (1-bit) ;
    [] pour enregistrer une image au format portable pixmaps : menu [,]Fichier, Exporter comme... et choisissez l'extension adaptée (monImage.pbm pour une image en noir  blanc, monImage.pgm pour une image en niveaux de gris ou monImage.ppm pour une image en couleurs).
Pour visualiser ces images sans avoir à démarrer (ce qui peut etre long sur des ordinateurs un peu anciens), n'importe quelle bonne visionneuse fera l'affaire. Sous Linux, aucun souci. Cela m'étonnerait qu'il y en ait avec MacOS. Quant à Windows, http://www.irfanview.com/IrfanView est gratuit et fonctionne parfaitement.







Comparer visuellement 2 images à la recherche d'informations cachées

Vous disposez de 2 images apparemment identiques (...plus ou moins !), et vous souhaitez savoir si elles le sont réellement, ou s'il y a des chances que l'une des deux contienne des informations cachées par stéganographie. Vous pouvez utiliser , c'est très simple !

    Ouvrez une image (n'importe laquelle).
    Ouvrez l'autre image en tant que calque : menu [,]Fichier, Ouvrir en tant que calques.... Il est aussi possible d'utiliser le raccourci clavier [+]Ctrl + Alt + O.
            Dans la fenêtre flottante de gestion des calques (si elle n'est pas visible, vous pouvez la faire apparaître en allant dans le menu [,]Fenêtres, Fenêtres ancrables, Calques), changez le mode du calque le plus haut pour Différence.
        
        0pt
                

            
                            Il faut ensuite aplatir l'image (ce qui revient à fusionner les calques) : [,]Image, Aplatir l'image.
    Pour mettre en évidence les différences, on peut chercher à appliquer un filtre qui joue sur les seuils : [,]Couleurs, Seuil.... Positionnez le curseur à des  valeurs basses(Il faudra mettre le curseur à 1 dans le cas où l'on travaillerait avec le code, [code:cacherMieux]donné en annexe, qui agit sur le de poids faible de chacune des valeurs des pixels de l'image conteneur.) et validez. Vous obtiendrez alors une image qui pourrait ressembler à l'une de celles qui suivent, où les différences entre les deux fichiers suspects sont nettement mises en lumière :
    
        [width=8cm]GimpDifférencesImageCachantMieuxUneImage.png
            


Installer https://python-pillow.org/Pillow dans un lycée de l'académie d'Orléans-Tours

La bibliothèque https://python-pillow.org/Pillow fournit des fonctionnalités largement semblables à PIL. C'est par elle qu'il faut passer si l'on veut recycler du code Python 2.x reposant sur PIL en Python 3.x. En effet, PIL n'est plus développé et n'a pas été porté sous Python 3.x. Un https://fr.wikipedia.org/wiki/Fork_28dC3A9veloppement_logiciel29fork a donc été initié, le code de PIL a été repris et amélioré, le nom a été modifié. Pour des besoins simples, rien ne change - d'ailleurs, on charge Pillow avec la commande pythonimport PIL : c'est dire ! Vous pouvez consulter la http://pillow.readthedocs.io/en/4.1.x/documentation officielle pour des informations exhaustives sur https://python-pillow.org/Pillow, ou http://www.pythonforbeginners.com/gui/how-to-use-pillowcet article pour une brève introduction.


Sous Windows, vous pourrez procéder comme suit si https://python-pillow.org/Pillow n'est pas présent dans votre installation Python. Ouvrez une console en ligne de commande en pressant simultanément [,]
[scale=.2, anchor=base][draw=white, fill=black, thick] (0,0) - (1,0) - (1,1) - (0,1) - cycle ;
      [draw=white, fill=black, thick] (1,0) - (2,0) - (2,1) - (1,1) - cycle ;
      [draw=white, fill=black, thick] (0,1) - (1,1) - (1,2) - (0,2) - cycle ;
      [draw=white, fill=black, thick] (1,1) - (2,1) - (2,2) - (1,2) - cycle ; , R , saisissez cmd puis validez.
Tapez alors les commandes ci-dessous (la ligne liée au proxy n'est pertinente que dans le contexte d'un lycée, à votre domicile elle est très probablement sans objet) :


    set HTTPSPROXY=proxy:8080

    cd C:Python34Scripts

    pip install --upgrade pip

    pip install pillow



















[^stéganographie-vs-cryptographie]:
    Cela signifie qu'on peut exposer au vu et au su de tous une image qui
    en cacherait une autre par un procédé de stéganographie. Il s'agit bien
    d'une forme de camouflage, mais au lieu de reposer sur l'ombre (et tout
    le monde sait que ce qui est tapi dans l'ombre est immédiatement suspect),
    on expose le secret à la lumière !    
    Pour plus de sécurité, il est en outre tout à fait possible de combiner
    stéganographie et cryptographie : on écrit un message, on le chiffre
    (ce qui le rend inintelligibles), on transforme la suite incompréhensible
    de symboles ainsi obtenue en image, que l'on cache ensuite dans une
    autre image :sunglasses: !

[^tatouage-commerce]:
    Dans [ce rapport d'activité de l'institut Carnot-LSI](https://issuu.com/floralis/docs/rapport_d_activite_lsi){target='__blank' rel='noopener'}, 
    page 27, vous trouverez une trace de leur offre en la matière, dès 2011 :
    ![](images/carnot-LSI-tatouage.png)

[^tatouage-traitres]:
    En 1986, Margaret THATCHER, Première Ministre du ROYAUME-UNI, lassée
    des fuites d'informations dans la presse, aurait exigé que les traitements
    de textes de son cabinet soient configurés de sorte que l'identité des
    utilisateurs soit encodée via les valeurs d'espacement de leurs textes.
    En cas de fuite, il devenait ainsi possible d'identifier le coupable...
    
    D'autres équipes travaillent sur ces domaines, y compris en FRANCE.
    Vous pouvez trouver, par exemple dans ce [rapport du Laboratoire GIPSA](http://www.gipsa-lab.grenoble-inp.fr/fileadmin/gipsa/lab_presentation/rapport_gipsa_2009-2014.pdf){target='__blank' rel='noopener'}
    (pour Grenoble Images Parole Signal Automatique, laboratoire mixte issu
    notamment du CNRS et de l'Université de Grenoble-Alpes), page 139, la
    mention du « traçage de traîtres ».
    
    Dans le même esprit, vous pourrez peut-être observer d'étranges et discrets
    pixels jaunes sur certaines impressions ou sur des documents papiers
    que vous receve(re)z, en provenance d'administrations ou d'entreprises.
    Là encore, l'objectif est de tracer les documents (et leurs créateurs).
    Vous pourrez [lire cet article de Tuxicoman : « les métadonnées de vos
    impressions couleurs »](https://tuxicoman.jesuislibre.net/2015/05/les-metadonnees-de-vos-impressions-couleurs.html){target='__blank' rel='noopener'}.
    Moralité : malheur aux lanceurs d'alertes trop naïfs ! Il est **vraiment**
    important d'être conscient de l'impact qu'ont les technologies numériques
    sur le monde dans lequel on vit...

[^Pillow]:
    Magie à laquelle on devra tout de même recourir par la suite, une fois les mécanismes à l'œuvre raisonnablement compris !

[gimp]: https://www.gimp.org/downloads/
